import { sidebar } from "vuepress-theme-hope";

export default sidebar({
  "/": [
    "",
    {
      text: "Electron 基础",
      icon: "book",
      prefix: "base/",
      link: "base/",
      children: "structure",
    }
  ],
});
