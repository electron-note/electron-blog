import { navbar } from "vuepress-theme-hope";

export default navbar([
  "/",
  {
    text: "Electron 基础",
    icon: "lightbulb",
    prefix: "base/",
    link:"base/"
  }
]);
