import { defineUserConfig } from "vuepress";
import { viteBundler } from "@vuepress/bundler-vite";
import theme from "./theme.js";

export default defineUserConfig({
  base: "/",
  bundler: viteBundler(),
  lang: "zh-CN",
  title: "electron note",
  description: "electron 笔记",

  theme,

  port: 7000,


  // 和 PWA 一起启用
  // shouldPrefetch: false,
});
