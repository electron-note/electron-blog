---
home: true
icon: home
title: 主页
heroImage: https://theme-hope-assets.vuejs.press/logo.svg
bgImage: https://theme-hope-assets.vuejs.press/bg/6-light.svg
bgImageDark: https://theme-hope-assets.vuejs.press/bg/6-dark.svg
bgImageStyle:
  background-attachment: fixed
heroText: electron blog
tagline: electron 笔记
actions:
  - text: Electron 基础
    link: ./base/
    type: primary

footer: 源码仓库 <a href="https://gitlab.com/electron-note" target="_blank">electron note</a> | MIT 协议, 可随意转载
---

本文档致力于描述`electron`的学习流程,期间涉及到的内容主要包括:
- `electron` 的基本使用

