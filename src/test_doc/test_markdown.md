---
title: 测试 markdown
icon: fab fa-markdown
date: 2024-01-27
footer: 源码仓库 <a href="https://gitcode.com/m0_53402432/compiler-guide" target="_blank">compiler-guide</a> | MIT 协议, 可随意转载
---

## 内置组件
### Badge
- <Badge text="tip" type="tip" vertical="middle" />
- <Badge text="warning" type="warning" vertical="middle" />
- <Badge text="danger" type="danger" vertical="middle" />
- <Badge text="info" type="info" vertical="middle" />
- <Badge text="note" type="note" vertical="middle" />


## 代码块
```ts {1,6-8}
import type { UserConfig } from "vuepress/cli";
import { defaultTheme } from "@vuepress/theme-default";

export const config: UserConfig = {
  title: "你好， VuePress",

  theme: defaultTheme({
    logo: "https://vuejs.org/images/logo.png",
  }),
};
```

## 代码选项卡

::: code-tabs

@tab version.h

```cpp
#ifndef GRAVER_CONFIGURATION_VERION_H_
#define GRAVER_CONFIGURATION_VERION_H_

namespace graver {
#define GRAVER_VERSION_MAJOR_COPY (GRAVER_VERSION_MAJOR)
#define GRAVER_VERSION_MINOR_COPY (GRAVER_VERSION_MINOR)
#define GRAVER_VERSION_PATCH_COPY (GRAVER_VERSION_PATCH)

#define GRAVER_VERSION_STR(R) #R
#define GRAVER_VERSION_STR2(R) GRAVER_VERSION_STR(R)

int get_graver_version_major();
int get_graver_version_minor();
int get_graver_version_patch();
};  // namespace graver
#endif

```

@tab version.cpp

```cpp
#include "graver/configuration/version.h"

#include <cstdlib>

#include "graver/configuration/configuration.h"

namespace graver {
int get_graver_version_major() {
    return static_cast<int>(strtol(GRAVER_VERSION_STR2(GRAVER_VERSION_MAJOR), nullptr, 10));
}
int get_graver_version_minor() {
    return static_cast<int>(strtol(GRAVER_VERSION_STR2(GRAVER_VERSION_MINOR), nullptr, 10));
}
int get_graver_version_patch() {
    return static_cast<int>(strtol(GRAVER_VERSION_STR2(GRAVER_VERSION_PATCH), nullptr, 10));
}
};  // namespace graver
```

:::



## 脚注

第一个脚注示例[^footnote_001]

第二个脚注示例[^footnote_002]


## 提示容器
::: important
重要容器。
:::

::: info
信息容器。
:::

::: note
注释容器。
:::

::: tip
提示容器
:::

::: warning
警告容器
:::

::: caution
危险容器
:::

::: details
详情容器
:::


## markmap

````markmap
---
markmap:
  colorFreezeLevel: 2
---

# markmap

## 链接

- <https://markmap.js.org/>
- [GitHub](https://github.com/markmap/markmap)

## 功能

- 链接
- **强调** ~~删除线~~ *斜体* ==高亮==
- 多行
  文字
- `行内代码`
-
    ```js
    console.log('code block');
    ```
- Katex
  - $x = {-b \pm \sqrt{b^2-4ac} \over 2a}$
- 现在我们可以通过 `maxWidth` 选项自动换行非常非常非常非常非常非常非常非常非常非常长的内容
````


## mermaid
```sequence Greetings
Alice ->> Bob: Hello Bob, how are you?
Bob-->>John: How about you John?
Bob--x Alice: I am good thanks!
Bob-x John: I am good thanks!
Note right of John: Bob thinks a long<br/>long time, so long<br/>that the text does<br/>not fit on a row.

Bob-->Alice: Checking with John...
Alice->John: Yes... John, how are you?
```


## flowchart
```flow:ant
st=>start: 开始|past:>http://www.google.com[blank]
e=>end: 结束|future:>http://www.google.com
op1=>operation: 操作1|past
op2=>operation: 操作2|current
sub1=>subroutine: 子程序|invalid
cond=>condition: 是/否?|approved:>http://www.google.com
c2=>condition: 判断2|rejected
io=>inputoutput: 进行反思...|future

st->op1(right)->cond
cond(yes, right)->c2
cond(no)->sub1(left)->op1
c2(yes)->io->e
c2(no)->op2->e
```

## 任务列表
- [ ] 计划 A
- [x] 计划 B

## 图片
![](../assets/imgs/test_doc/avatar.jpg)

![](../assets/imgs/test_doc/avatar.jpg =180x180)


## badgen
> 参考: [工具资源系列之 github 上各式各样的小徽章从何而来?](https://www.cnblogs.com/snowdreams1006/p/11068107.html)

![](https://badgen.net/badge/subject/status/007EC6?scale=1) ![](https://badgen.net/badge/version/1.0.0/007EC6?scale=1) 





---
[^footnote_001]: 些许内容

[^footnote_002]: 些许内容